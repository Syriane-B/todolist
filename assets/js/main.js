//tab declaration
let toDoList = [];
let $toDoList = document.getElementById('toDoList');

function checkChecked() {
    document.getElementById('checkedCount').innerText = document.querySelectorAll('.checked').length;
}
//removing item from the tab function DON'T USE BEFORE RELOAD FUNCTION DECLARATION
function remove(index) {
    toDoList.splice(index, 1);
    reload();
    checkChecked();
}

//show on Dom toDoList tab content
function reload() {
    //emptying current Dom list
    $toDoList.innerHTML = '';
    //show nbr of toDoItems
    document.getElementById('toDoCount').innerText = toDoList.length;
    //hide the toDoListContainer if todolist is empty
    if (toDoList.length == 0) {
        document.getElementById('toDoListContainer').classList.add('hide');
        document.getElementById('toDoInput').classList.add('centered');
    }
    //for each tab element, add this template to Dom
    toDoList.forEach(function (el, index) {
        let newToDoItem = document.createElement('li');
        newToDoItem.innerHTML =
            `
            <input type="checkbox" name="${index}Txt" id="${index}Txt">
            <label class="" for="${index}Txt">${el.val}</label>
            <button id="${index}" class="closeBtn btn"> X </button>
        `
        $toDoList.appendChild(newToDoItem);
        //if checked add checked class
        if (el.checked === true) {
            document.getElementById(`${index}Txt`).checked = true;
            document.getElementById(`${index}Txt`).nextElementSibling.classList.add('checked');
        }
        //active close btn
        document.getElementById(index).addEventListener('click', function () {
            remove(index);
        })
        //active checked box on click
        document.getElementById(`${index}Txt`).addEventListener('click', function (e) {

            if (e.target.checked) {
                e.target.nextElementSibling.classList.add('checked');
                toDoList[index].checked = true;
            }
            else {
                e.target.nextElementSibling.classList.remove('checked');
                toDoList[index].checked = false;
            }
            checkChecked();
        })
    });
}

//value addition to tab
document.getElementById('toDoInput').addEventListener('submit', function (e) {
    //don't send the form
    e.preventDefault();
    //Show the to do list
    document.getElementById('toDoListContainer').classList.remove('hide');
    document.getElementById('toDoInput').classList.remove('centered');
    //add the vatue to the tab
    toDoList.push({ "val": document.getElementById('toDo').value, "checked": "false" });
    //emptying input
    document.getElementById('toDo').value = '';
    //reload dom with tab value
    reload();
});

//Pomodoro counter

//récupère les éléments du DOM avec leur Id
let $mCnt = document.getElementById('mCnt');
let $secCnt = document.getElementById('secCnt');

//crée le tableau du cycle des minutes
let mCycle = [25, 3, 25, 3, 25, 3, 25, 15];
//crée l'index qui parcours le tableau, il démarre à 0
let indexmCycle = 0;
//crée le compteur minute qui récupère la valeur de l'index dans le tableau des valeurs
let mCnt = mCycle[indexmCycle];
//crée le compteur des secondes qui démarre à 0
let secCnt = 00;
let counting;
function switchWorkPauseClass() {
    document.getElementById('cnt').classList.toggle('work');
    document.getElementById('cnt').classList.toggle('pause');
}

function uploadMinDom() {
    if (mCnt < 10) {
        $mCnt.innerText = '0' + mCnt;
    }
    else {
        $mCnt.innerText = mCnt;
    }
}

//crée la fonction qui décrémente les secondes et les minutes
function counter() {
    //quand les secondes sont à 0 : remet 59 sec et met à jour dans le DOM
    if (secCnt == 0) {
        secCnt = 59;
        $secCnt.innerText = secCnt;
        //quand les minutes sont à 0 : parcours le cycle du tableau
        if (mCnt == 0) {
            //quand le cycle du tableau arrive à la longeur du tableau; recommence à 0
            if (indexmCycle == (7)) {
                indexmCycle = 0;
                switchWorkPauseClass();
                //règle le compteur minute à la valeur du tableau
                mCnt = mCycle[indexmCycle];
                //enlève 1 minute
                --mCnt;
                //met à jour dans le DOM en ajoutant un 0 devant si besoin
                uploadMinDom();
            }
            else {
                //quand l'index n'est pas égal à 7, on incrémente l'index de 1
                ++indexmCycle;
                switchWorkPauseClass();
                //règle le compteur minute à la valeur du tableau
                mCnt = mCycle[indexmCycle];
                //enlève 1 minute
                --mCnt;
                //met à jour dans le DOM en ajoutant un 0 devant si besoin
                uploadMinDom();
            }
        }
        else {
            //quand le compteur minute n'est pas égal à 0 : on décrémente les minutes
            --mCnt;
            //met à jour le compteur dans le DOM
            uploadMinDom();
        }
    }
    else {
        //quand les secondes ne sont pas égales à 0 : on décrémente les secondes
        --secCnt;
        //met à jour dans le DOM en ajoutant un zéro devant si besoin
        if (secCnt < 10) {
            $secCnt.innerText = '0' + secCnt;
        }
        else {
            $secCnt.innerText = secCnt;
        }
    }
}

//on écoute le click sur le bouton start : apelle la fonction counter, toutes les secondes
document.getElementById('startBtn').addEventListener('click', function () {
    clearInterval(counting);
    counting = setInterval(counter, 1000);
});
//on écoute le click sur le bouton pause : mettre en pause les intervalles
document.getElementById('pauseBtn').addEventListener('click', function () {
    clearInterval(counting);
});